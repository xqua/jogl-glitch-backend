class AddWorkgroupToProjects < ActiveRecord::Migration[6.1]
  def change
    add_column :projects, :workgroup_id, :int
  end
end
