class CreateServices < ActiveRecord::Migration[6.1]
  def change
    create_table :services do |t|
      t.column :name, :integer, default: 0, null: false
      t.bigint :creator_id, null: false
      t.bigint :serviceable_id, null: false
      t.string :serviceable_type, null: false
      t.string :token, null: false
      t.datetime :created_at, null: false, default: ->{ 'now()' }
      t.datetime :updated_at, null: false, default: ->{ 'now()' }
    end

    add_index :services, [:name, :serviceable_id, :serviceable_type], unique: true, name: 'unique_serviceable_index'
  end
end
