class CreateSpacesTable < ActiveRecord::Migration[5.2]
  def change
    create_table :spaces do |t|
      t.string :description
      t.string :title
      t.datetime :launch_date
      t.datetime :end_date
      t.timestamps
      t.string :short_title
      t.string :faq
      t.string :enablers
      t.string :ressources
      t.string :short_description
      t.string :title_fr
      t.string :short_title_fr
      t.string :description_fr
      t.string :short_description_fr
      t.string :faq_fr
      t.string :enablers_fr
      t.integer :status, default: 0, null: false
      t.string :contact_email
      t.string :meeting_information
    end

    add_column :programs, :space_id, :bigint
    add_index :spaces, :status
  end
end
