class AddUniqueIndexToAffiliation < ActiveRecord::Migration[6.1]
  def change
    fields = [:affiliate_id, :affiliate_type, :parent_id, :parent_type]
    add_index :affiliations, fields, unique: true, name: 'unique_affiliateable_index'
  end
end
