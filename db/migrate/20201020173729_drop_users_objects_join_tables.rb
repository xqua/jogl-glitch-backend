# frozen_string_literal: true

class DropUsersObjectsJoinTables < ActiveRecord::Migration[5.2]
  def change
    drop_table :users_communities
    drop_table :users_needs
    drop_table :users_programs
    drop_table :users_projects
    drop_table :users_spaces
    drop_table :challenges_users # the one random join table that broke the pattern lol
    # NOTE: users_boards stays for now, though I don't think it's actually used
  end
end
