class AddGrantInfoToProjects < ActiveRecord::Migration[6.1]
  def change
    add_column :projects, :grant_info, :string
  end
end
