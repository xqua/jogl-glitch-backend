class CreateActivities < ActiveRecord::Migration[6.1]
  def change
    create_table :activities, id: false do |t|
      t.bigint :service_id, null: false, index: true, foreign_key: true
      t.string :id, null: false
      
      t.integer :category, null: false
      t.string :name, null: false
      t.text :description
      t.datetime :start
      t.datetime :end
      t.string :location
      t.float :latitude
      t.float :longitude
      t.string :url
      
      t.timestamps default: -> { 'now()' }
    end

    add_index "activities", [:service_id, :id], unique: true
    
    create_table :activities_skills do |t|
      t.belongs_to :activity
      t.belongs_to :skill
    end
  end
end
