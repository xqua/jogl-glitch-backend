class CreateProposals < ActiveRecord::Migration[6.1]
  def change
    create_table :proposals do |t|
      t.references :peer_review, null: false
      t.references :project, null: false

      t.string :funding, limit: 1024
      t.float :score
      t.string :short_title
      t.datetime :submitted_at
      t.string :summary
      t.string :title

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    create_table :interests_proposals, id: false do |t|
      t.integer :interest_id
      t.integer :proposal_id
    end

    create_table :proposals_skills, id: false do |t|
      t.integer :proposal_id
      t.integer :skill_id
    end

    add_index :proposals, %i[peer_review_id project_id], unique: true
  end
end
