# frozen_string_literal: true

class CreateData < ActiveRecord::Migration[5.2]
  def change
    create_table :data do |t|
      t.string :title
      t.string :description
      t.string :author_id
      t.string :url
      t.string :format
      t.string :filename

      t.bigint :dataset_id

      t.string :provider_id
      t.string :provider_version
      t.datetime :provider_created_at
      t.datetime :provider_updated_at

      t.timestamps
    end
  end
end
