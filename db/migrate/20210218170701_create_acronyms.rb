class CreateAcronyms < ActiveRecord::Migration[6.1]
  def change
    create_table :acronyms do |t|
      # Add the acronym table for NLP
      t.string :acronym_name
      t.string :probable_skill_name
      t.float :probability
      t.boolean :is_in_parenthesis
      t.integer :count
      t.timestamps
    end
  end
end
