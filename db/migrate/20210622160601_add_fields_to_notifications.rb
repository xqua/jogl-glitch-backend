class AddFieldsToNotifications < ActiveRecord::Migration[6.1]
  def change
    add_column :notifications, :cta_link, :string
    add_column :notifications, :subject_line, :string, null: false, default: ''
  end
end
