class FixingChallengeError < ActiveRecord::Migration[6.1]
  def change
    add_column :challenges, :space_id, :bigint
    remove_column :programs, :space_id
  end
end
