class AddUniqueIndexToMention < ActiveRecord::Migration[6.1]
  def change
    fields = [:obj_id, :obj_type, :post_id]
    add_index :mentions, fields, unique: true, name: 'unique_mention_index'
  end
end