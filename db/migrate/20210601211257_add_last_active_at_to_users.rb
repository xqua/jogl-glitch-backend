class AddLastActiveAtToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :last_active_at, :datetime, default: -> { 'CURRENT_TIMESTAMP' }
  end
end
