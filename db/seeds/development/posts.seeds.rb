# frozen_string_literal: true

after 'development:users', 'development:projects' do
  feed = Feed.find_by(feedable_type: 'Project')
  from = feed.feedable
  user = User.first

  3.times do
    feed.posts << Post.create!(user: user, feed: feed, from: from, from_name: from.title, content: FFaker::Book.title)
  end
end
