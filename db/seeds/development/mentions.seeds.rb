# frozen_string_literal: true

after 'development:users', 'development:posts', 'development:comments' do
  Mention.create!(post_id: Post.last.id, obj_type: 'User', obj_id: User.first.id, obj_match: User.first.nickname)
end
