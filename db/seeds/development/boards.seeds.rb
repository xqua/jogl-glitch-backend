# frozen_string_literal: true

after 'development:users', 'development:programs' do
  board = Board.create!(title: FFaker::Book.title, description: FFaker::Book.description)

  board.users << User.first

  Program.first.boards << board
end
