# frozen_string_literal: true

after 'development:users', 'development:programs', 'development:projects' do
  need = Need.create!(project_id: Project.first.id, user_id: User.first.id, title: FFaker::Book.title, content: FFaker::Book.description)

  need.skills << Skill.create!(skill_name: FFaker::Skill.specialty)
  need.ressources << Ressource.create!(ressource_name: FFaker::Product.product_name)

  User.first.add_role(:admin, need)
  User.first.add_role(:member, need)
  User.first.add_role(:owner, need)
end
