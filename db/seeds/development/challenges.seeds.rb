# frozen_string_literal: true

after 'development:users', 'development:programs', 'development:spaces' do
  challenge = Challenge.create!(title: FFaker::Book.title, short_title: FFaker::Book.title)

  User.first.add_role(:admin, challenge)

  Program.first.challenges << challenge
  Space.first.challenges << challenge
end
