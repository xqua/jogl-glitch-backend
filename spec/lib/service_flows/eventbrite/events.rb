# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

RSpec.describe ServiceFlows::Eventbrite::Events do
  let(:service_flow) {
    service = create(:service)
    ServiceFlowRegistry.load!(service, :events)
  }
    
  it 'fetches the service flow data correctly' do
    expect { service_flow.fetch_data }.to change { ServiceFlowData.count }.by 1
  end

  it 'only stores service flow data once for the same service flow' do
    expect { 2.times { service_flow.fetch_data } }.to change { ServiceFlowData.count }.by 1
  end

  it 'loads the service flow data correctly' do
    service_flow.fetch_data
    service_flow_data = ServiceFlowData.first.data

    expect(service_flow_data).to be_a(Hash)
  end

  it 'copies service flow data into a new activity' do
    service_flow.fetch_data
    service_flow_data = ServiceFlowData.first.data
    id = service_flow_data.values.first.first[:id]

    expect { service_flow.copy_data(ids: [id]) }.to change { Activity.count }.by 1
  end
end
