# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ServiceFlowRegistry do
  it 'returns a hash of service flows' do
    expect(described_class.service_flows).to be_a(Hash)
  end

  it 'returns flows for a given service' do
    service = create(:service, name: :eventbrite)
    flows = described_class.flows(service)

    expect(flows).to be_a(Hash)
  end

  it 'loads a service flow given the service and flow' do
    service = create(:service, name: :eventbrite)
    service_flow = described_class.load!(service, :events)

    expect(service_flow).to be_a(ServiceFlow)
  end
end
