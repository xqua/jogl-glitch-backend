# frozen_string_literal: true

require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe GeocoderWorker, type: :worker do
  before(:all) do
    @worker = GeocoderWorker.new
  end

  it 'geocodes a user by ip address when no city is present' do
    user = create(:user, ip: '99.51.184.219', city: nil)
    @worker.perform('User', user.id)

    expect(user.latitude).to_not be_nil
    expect(user.longitude).to_not be_nil
  end

  it 'geocodes a full address' do
    user = create(:user, address: '10414 McKalla Place', city: 'Austin', country: 'United States')
    @worker.perform('User', user.id)

    expect(user.latitude).to_not be_nil
    expect(user.longitude).to_not be_nil
  end

  it 'geocodes a city/country' do
    user = create(:user, ip: nil, address: nil, city: 'Austin', country: 'United States')
    @worker.perform('User', user.id)

    expect(user.latitude).to_not be_nil
    expect(user.longitude).to_not be_nil
  end
end
