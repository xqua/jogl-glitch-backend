# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with affiliations' do |factory|
  before do
    @object = create(factory)
    @parent = create(:parent)
  end

  context 'Signed in' do
    before(:each) do
      @user = create(:confirmed_user)
      sign_in @user
    end

    describe '#affiliation_index' do
      it 'should return the list of affiliations for the object' do
        @object.add_affiliation_to(@parent)
        get :affiliation_index, params: {id: @object.id}
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['parents'].count).to eq(1)
        expect(json_response['parents'].first['parent_id']).to eq(@parent.id)
      end

      it 'should be empty if there is no affiliations' do
        get :affiliation_index, params: {id: @object.id}
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['parents'].count).to eq(0)
      end

      it 'should return the correct status for the affiliations' do
        @object.add_affiliation_to(@parent)
        get :affiliation_index, params: {id: @object.id}
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['parents'].count).to eq(1)
        expect(json_response['parents'].first['status']).to eq('pending')
      end
    end

    describe '#affiliation_show' do
      # get '/:id/affiliations/:parent_type/:parent_id' => :affiliation_show
      it 'should return an affiliation with the correct information' do
        @object.add_affiliation_to(@parent)

        get :affiliation_show, params: {
          id: @object.id,
          parent_type: @parent.class.name.pluralize.downcase,
          parent_id: @parent.id
        }

        expect(response).to have_http_status :ok

        json_response = JSON.parse(response.body)

        expect(json_response['parent_id']).to eq(@parent.id)
        expect(json_response['status']).to eq('pending')
      end

      it 'should retun not found if there is no affiliation' do
        get :affiliation_show, params: {
          id: @object.id,
          parent_type: @parent.class.name.pluralize.downcase,
          parent_id: @parent.id
        }
        expect(response).to have_http_status :not_found
      end
    end

    describe '#affiliation_create' do
      # post '/:id/affiliations/:parent_type/:parent_id' => :affiliation_create
      it 'should NOT create an affiliation' do
        expect do
          post :affiliation_create, params: {
            id: @object.id,
            parent_type: @parent.class.name.pluralize.downcase,
            parent_id: @parent.id
          }
        end.to change(Affiliation, :count).by(0)
        expect(response).to have_http_status :forbidden
      end
    end

    describe '#affiliation_update' do
      # patch '/:id/affiliations/:parent_type/:parent_id' => :affiliation_update
      it 'should NOT update the affiliation' do
        @object.add_affiliation_to(@parent)
        patch :affiliation_update, params: {
          affiliations: {status: 'accepted'},
          id: @object.id,
          parent_type: @parent.class.name.pluralize.downcase,
          parent_id: @parent.id
        }
        expect(response).to have_http_status :forbidden
        expect(Affiliation.find_by(parent: @parent, affiliate: @object).status).to eq('pending')
      end
    end

    describe '#affiliation_destroy' do
      # delete '/:id/affiliations/:parent_type/:parent_id' => :affiliation_destroy
      it 'should NOT destroy the affiliation' do
        @object.add_affiliation_to(@parent)
        expect do
          delete :affiliation_destroy, params: {
            id: @object.id,
            parent_type: @parent.class.name.pluralize.downcase,
            parent_id: @parent.id
          }
        end.to change(Affiliation, :count).by(0)
        expect(response).to have_http_status :forbidden
      end
    end
  end

  context 'Is Object admin or is Self' do
    before(:each) do
      unless @object.class.name == "User"
        @user = create(:confirmed_user)
        sign_in @user
        @object.users << @user
        @user.add_role :member, @object
        @user.add_role :admin, @object
      else
        sign_in @object
      end
    end

    describe '#affiliation_create' do
      it 'should create an affiliation' do
        expect do
          post :affiliation_create, params: {
            id: @object.id,
            parent_type: @parent.class.name.pluralize.downcase,
            parent_id: @parent.id
          }
        end.to change(Affiliation, :count).by(1)
        expect(response).to have_http_status :created
        expect(Affiliation.where(parent: @parent, affiliate: @object)).to exist
      end

      it 'should create not create an affiliation if it exists' do
        @object.add_affiliation_to @parent
        expect do
          post :affiliation_create, params: {
            id: @object.id,
            parent_type: @parent.class.name.pluralize.downcase,
            parent_id: @parent.id
          }
        end.to change(Affiliation, :count).by(0)
      end

      it 'should have a default status pending' do
        post :affiliation_create, params: {
          id: @object.id,
          parent_type: @parent.class.name.pluralize.downcase,
          parent_id: @parent.id
        }
        expect(Affiliation.find_by(parent: @parent, affiliate: @object).status).to eq('pending')
      end
    end

    describe '#affiliation_update' do
      it 'should NOT update the affiliation' do
        @object.add_affiliation_to(@parent)
        patch :affiliation_update, params: {
          affiliations: {status: 'accepted'},
          id: @object.id,
          parent_type: @parent.class.name.pluralize.downcase,
          parent_id: @parent.id
        }
        expect(response).to have_http_status :forbidden
        expect(Affiliation.find_by(parent: @parent, affiliate: @object).status).to eq('pending')
      end
    end

    describe '#affiliation_destroy' do
      it 'should destroy the affiliation' do
        @object.add_affiliation_to(@parent)
        expect do
          delete :affiliation_destroy, params: {
            id: @object.id,
            parent_type: @parent.class.name.pluralize.downcase,
            parent_id: @parent.id
          }
        end.to change(Affiliation, :count).by(-1)
        expect(response).to have_http_status :ok
        expect(Affiliation.where(parent: @parent, affiliate: @object)).not_to exist
      end
    end
  end

  context 'Is Parent admin' do
    before(:each) do
      @user = create(:confirmed_user)
      sign_in @user
      @parent.users << @user
      @user.add_role :member, @parent
      @user.add_role :admin, @parent
    end

    describe '#affiliation_update' do
      it 'should update the affiliation' do
        @object.add_affiliation_to(@parent)
        patch :affiliation_update, params: {
          affiliations: {status: 'accepted'},
          id: @object.id,
          parent_type: @parent.class.name.pluralize.downcase,
          parent_id: @parent.id
        }
        expect(response).to have_http_status :ok
        expect(Affiliation.find_by(parent: @parent, affiliate: @object).status).to eq('accepted')
      end
    end
  end

  context 'Not Signed In' do
    describe '#affiliation_index' do
      it 'should return the list of affiliations for the object' do
        @object.add_affiliation_to(@parent)
        get :affiliation_index, params: {id: @object.id}
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['parents'].count).to eq(1)
        expect(json_response['parents'].first['parent_id']).to eq(@parent.id)
      end

      it 'should be empty if there is no affiliations' do
        get :affiliation_index, params: {id: @object.id}
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['parents'].count).to eq(0)
      end

      it 'should return the correct status for the affiliations' do
        @object.add_affiliation_to(@parent)
        get :affiliation_index, params: {id: @object.id}
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['parents'].count).to eq(1)
        expect(json_response['parents'].first['status']).to eq('pending')
      end
    end

    describe '#affiliation_show' do
      # get '/:id/affiliations/:parent_type/:parent_id' => :affiliation_show
      it 'should return an affiliation with the correct information' do
        @object.add_affiliation_to(@parent)
        get :affiliation_show, params: {
          id: @object.id,
          parent_type: @parent.class.name.pluralize.downcase,
          parent_id: @parent.id
        }
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['parent_id']).to eq(@parent.id)
        expect(json_response['status']).to eq('pending')
      end

      it 'should retun not found if there is no affiliation' do
        get :affiliation_show, params: {
          id: @object.id,
          parent_type: @parent.class.name.pluralize.downcase,
          parent_id: @parent.id
        }
        expect(response).to have_http_status :not_found
      end
    end

    describe '#affiliation_create' do
      # post '/:id/affiliations/:parent_type/:parent_id' => :affiliation_create
      it 'should NOT create an affiliation' do
        expect do
          post :affiliation_create, params: {
            id: @object.id,
            parent_type: @parent.class.name.pluralize.downcase,
            parent_id: @parent.id
          }
        end.to change(Affiliation, :count).by(0)
        expect(response).to have_http_status :unauthorized
      end
    end

    describe '#affiliation_update' do
      # patch '/:id/affiliations/:parent_type/:parent_id' => :affiliation_update
      it 'should NOT update the affiliation' do
        @object.add_affiliation_to(@parent)
        patch :affiliation_update, params: {
          affiliations: {status: 'accepted'},
          id: @object.id,
          parent_type: @parent.class.name.pluralize.downcase,
          parent_id: @parent.id
        }
        expect(response).to have_http_status :unauthorized
        expect(Affiliation.find_by(parent: @parent, affiliate: @object).status).to eq('pending')
      end
    end

    describe '#affiliation_destroy' do
      # delete '/:id/affiliations/:parent_type/:parent_id' => :affiliation_destroy
      it 'should NOT destroy the affiliation' do
        @object.add_affiliation_to(@parent)
        expect do
          delete :affiliation_destroy, params: {
            id: @object.id,
            parent_type: @parent.class.name.pluralize.downcase,
            parent_id: @parent.id
          }
        end.to change(Affiliation, :count).by(0)
        expect(response).to have_http_status :unauthorized
      end
    end
  end
end
