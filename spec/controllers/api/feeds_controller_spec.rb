# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::FeedsController, type: :controller do
  let(:user) { create(:confirmed_user) }
  let(:project) { create(:project, creator_id: user.id) }
  let(:program) { create(:program) }
  let(:feed) { create(:feed, feedable: project) }

  before do
    sign_in user
  end

  describe '#index' do
    context 'with logged in user' do
      before do
        user.feed.posts << create(:post, user: user, feed: user.feed, from: user)
        user.owned_relations.create(resource: program, follows: true)
        program.feed.posts << create(:post, from: program)

        get :index
      end

      it 'returns HTTP status ok' do
        expect(response).to have_http_status :ok
      end

      it 'returns the correct number of posts in the feed' do
        json_response = JSON.parse(response.body)

        expect(json_response.count).to eq(2)
      end

      it 'returns the posts in time descending order' do
        json_response = JSON.parse(response.body)

        expect(json_response[0]['content']).to eq(program.feed.posts.first.content)
      end
    end

    context 'with logged out user' do
      it 'does not return list of feed' do
        sign_out user
        get :index

        expect(response).to have_http_status :unauthorized
      end
    end
  end

  describe '#show' do
    let(:user_post) { create(:post, user: user, feed: user.feed, from: user) }

    it 'returns HTTP status ok' do
      get :show, params: { id: feed.id }

      expect(response).to have_http_status :ok
    end

    it 'returns allow_posting_to_all' do
      get :show, params: { id: user.feed.id }
      json_response = JSON.parse(response.body)

      expect(json_response['allow_posting_to_all']).to eq(true)
    end

    it 'returns posts' do
      user.feed.posts << user_post
      get :show, params: { id: user.feed.id }
      json_response = JSON.parse(response.body)

      expect(json_response['posts']).not_to be_empty
    end
  end

  describe '#update' do
    it 'is accessible to an admin' do
      patch :update, params: { id: feed.id }

      expect(response).to have_http_status :ok
    end

    it 'is not be accessible to a non-admin' do
      user.remove_role :admin, project
      patch :update, params: { id: feed.id }

      expect(response).to have_http_status :forbidden
    end

    it 'changes allow posting to all to false' do
      patch :update, params: { id: feed.id, allow_posting_to_all: false }
      feed.reload

      expect(feed.allow_posting_to_all).to eq(false)
    end
  end

  describe '#remove_post' do
    let(:project_post) { create(:post, user_id: user.id) }

    before do
      project.feed.posts << project_post
    end

    it 'returns HTTP status ok' do
      user.add_role :admin, project
      delete :remove_post, params: { id: project.feed.id, post_id: project_post.id }

      expect(response).to have_http_status :ok
    end

    it 'removes a post from a feed' do
      user.add_role :admin, project
      delete :remove_post, params: { id: project.feed.id, post_id: project_post.id }

      expect(project.posts_count).to eq 0
    end

    it 'is forbidden to not admin' do
      user.remove_role :admin, project
      delete :remove_post, params: { id: project.feed.id, post_id: project_post.id }

      expect(response).to have_http_status :forbidden
    end
  end
end
