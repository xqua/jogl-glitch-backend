# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::ServicesController, type: :controller do
  let(:user) { create(:confirmed_user) }
  let(:space) { create(:space) }
  let(:service) { create(:service, creator: user, serviceable_id: space.id, serviceable_type: 'Space') }

  before do
    user.add_role(:admin, space)
    sign_in user
  end

  describe '#fetch_service_flow_data' do
    it 'fetches the data for the service flow' do
      put :fetch_service_flow_data, params: { id: service.id, type: 'events' }

      expect(response).to have_http_status :created
    end
  end

  describe '#load_service_flow_data' do
    it 'loads the data for the service flow' do
      put :fetch_service_flow_data, params: { id: service.id, type: 'events' }
      get :load_service_flow_data, params: { id: service.id, type: 'events' }
      json_response = JSON.parse(response.body)

      expect(json_response).to include('service_id' => service.id, 'flow' => 'events')
    end
  end

  describe '#show' do
    it 'shows the correct service' do
      get :show, params: { id: service.id }
      json_response = JSON.parse(response.body)

      expect(json_response).to include('id' => service.id, 'creator_id' => user.id)
    end
  end

  describe '#index_flows' do
    it 'gets the flows for a service' do
      get :index_flows, params: { id: service.id }
      json_response = JSON.parse(response.body)

      expect(json_response).to be_an(Array)
    end
  end

  describe 'unauthorized' do
    it 'returns unauthorized for a non-authenticated user' do
      sign_out user

      post :copy_service_flow_data, params: { id: service.id, type: 'events', opts: {} }

      expect(response).to have_http_status :unauthorized
    end

    it 'returns unauthorized for a non-admin' do
      sign_in create(:user)

      post :copy_service_flow_data, params: { id: service.id, type: 'events', opts: {} }

      expect(response).to have_http_status :unauthorized
    end
  end
end
