# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::PagesController, type: :controller do
  before do
    @user = create(:confirmed_user)
    sign_in @user
  end

  describe '#index' do
    it 'should return list of Pages' do
      get :index
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(json_response['content']).to eq 'Welcome to the JOGL API'
    end
  end

  describe '#index' do
    it 'should return Unauthorized error' do
      sign_out @user
      get :index
      json_response = JSON.parse(response.body)
      expect(response).to have_http_status :ok
      expect(json_response['content']).to eq 'Welcome to the JOGL API'
    end
  end
end
