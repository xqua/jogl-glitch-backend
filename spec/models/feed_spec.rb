# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Feed, type: :model do
  it { should belong_to(:feedable) }

  it { should have_and_belong_to_many(:posts) }
end
