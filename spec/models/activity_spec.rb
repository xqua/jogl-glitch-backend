# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Activity, type: :model do
  subject { FactoryBot.build(:activity) }

  it { is_expected.to belong_to(:service) }

  it { is_expected.to define_enum_for(:category).with_values(%i[event]) }
end
