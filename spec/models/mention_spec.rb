# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mention, type: :model do
  it { is_expected.to belong_to(:obj) }
  it { is_expected.to belong_to(:post) }

  it { is_expected.to validate_uniqueness_of(:post_id).scoped_to(:obj_id, :obj_type).case_insensitive }

  it 'has a valid factory' do
    post = create(:post)
    mention = create(:mention, post: post, obj: post.user)

    expect(mention).to be_valid
  end

  describe 'when a mention is created with a post' do
    let!(:post) { create(:post) }
    let!(:mention) { create(:mention, post: post, obj: post.user) }

    it 'is invalid when referencing the same post' do
      mention2 = build(:mention, post: mention.post, obj: post.user)

      expect(mention2).not_to be_valid
    end
  end
end
