# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Authorship, type: :model do
  it { should belong_to(:authorable) }
  it { should belong_to(:user) }
end
