# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Comment, type: :model do
  it { should belong_to(:post) }
  it { should belong_to(:user) }

  it { should validate_presence_of(:content) }

  it 'has a valid factory' do
    comment = build(:comment)

    expect(comment).to be_valid
  end

  it 'returns a frontend_link if a post is present' do
    post = create(:post)
    comment = build(:comment, post: post)
    frontend_link = comment.frontend_link

    expect(frontend_link).to eq("/post/#{post.id}")
  end

  it 'returns a frontend_link of 404 if no post is present' do
    comment = build(:comment, post: nil)
    frontend_link = comment.frontend_link

    expect(frontend_link).to eq('/404')
  end
end
