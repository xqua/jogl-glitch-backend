# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Externalhook, type: :model do
  it { should belong_to(:hookable) }

  it { should have_many(:slackhooks) }
end
