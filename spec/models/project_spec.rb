# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/feedable'
require_relative 'shared_examples/affiliatable_child'
require_relative 'shared_examples/mediumable'

RSpec.describe Project, type: :model do
  it_behaves_like 'a model that can have a feed', :project
  it_behaves_like 'a model that is an affilatable child', :project
  it_behaves_like 'a model that has many media', :project

  it { should belong_to(:creator) }

  it { should have_and_belong_to_many(:interests) }
  it { should have_and_belong_to_many(:skills) }

  it { should have_many(:challenges) }
  it { should have_many(:challenges_projects) }
  it { should have_many(:customfields) }
  it { should have_many(:externalhooks) }
  it { should have_many(:needs) }
  it { should have_many(:needs_projects) }

  it { should have_many_attached(:documents) }

  it 'has a valid factory' do
    user = create(:user)
    project = create(:project, creator_id: user.id)

    expect(project).to be_valid
  end

  describe 'members' do
    it 'does something' do
      project = create(:project)
      expect(project.members).to include(project.creator)
      expect(project.members.size).to eq(1)
    end
  end
end
