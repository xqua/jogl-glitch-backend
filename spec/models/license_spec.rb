# frozen_string_literal: true

require 'rails_helper'

RSpec.describe License, type: :model do
  it { should belong_to(:licenseable) }
end
