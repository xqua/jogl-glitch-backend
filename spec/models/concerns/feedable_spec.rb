# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Feedable, type: :concern do
  let(:challenge) { FactoryBot.create(:challenge) }
  let(:space) { FactoryBot.create(:space) }

  it 'has a feed with allow_posting_to_all set to default true' do
    expect(challenge.feed.allow_posting_to_all).to be_truthy
  end

  context 'when a space' do
    it 'has a feed with allow_posting_to_all set to false' do
      expect(space.feed.allow_posting_to_all).to be_falsey
    end
  end
end
