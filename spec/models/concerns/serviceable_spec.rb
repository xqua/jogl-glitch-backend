# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Serviceable, type: :concern do
  subject { described_class.new }

  describe '#serviceable?' do
    let(:user) { FactoryBot.create(:user) }

    it 'returns false when the object class does not include serviceable' do
      expect(described_class.serviceable?('challenge', 1, user.id)).to be false
    end

    it 'returns false when the object does not exist' do
      expect(described_class.serviceable?('space', 1, user.id)).to be false
    end

    it 'returns false when the user does not exist' do
      space = FactoryBot.create(:space)

      expect(described_class.serviceable?('space', space.id, 2)).to be false
    end

    it 'returns true when object exists, object class is serviceable, and user exists' do
      space = FactoryBot.create(:space)

      expect(described_class.serviceable?('space', space.id, user.id)).to be true
    end
  end
end
