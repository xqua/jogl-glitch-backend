# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Document, type: :model do
  it { should have_many(:authorship) }
  it { should have_many(:users) }

  it { should validate_presence_of(:content) }
  it { should validate_presence_of(:title) }

  it 'has a valid factory' do
    document = build(:document)

    expect(document).to be_valid
  end
end
