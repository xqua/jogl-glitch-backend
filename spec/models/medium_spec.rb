# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Medium, type: :model do
  it { should belong_to(:mediumable) }

  it { should have_one_attached(:item) }

  it { should validate_presence_of(:title) }

  it 'has valid factories' do
    expect(build(:medium)).to be_valid
    expect(build(:medium, :for_project)).to be_valid
    expect(build(:medium, :for_workgroup)).to be_valid
    expect(build(:medium, :for_program)).to be_valid
    expect(build(:medium, :for_challenge)).to be_valid
    expect(build(:medium, :for_space)).to be_valid
    expect(build(:medium, :for_dataset)).to be_valid
  end

  it 'responds to #item_url' do
    medium = build(:medium)

    expect(medium).to respond_to(:item_url)
    expect { medium.item_url }.to_not raise_error
  end

  it 'responds to #item_url' do
    medium = build(:medium)

    expect(medium).to respond_to(:item_url_sm)
    expect { medium.item_url_sm }.to_not raise_error
  end

  it 'should generate a URL' do
    medium = build(:medium)

    expect(medium.item_url).not_to eq("")
  end

  it 'should generate a URL variant on images' do
    medium = build(:medium)

    expect(medium.item_url).not_to eq(medium.item_url_sm)
  end
end
