# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'WorkgroupApiUnprotected', type: :request do
  before do
    @creator = create(:user)
    [@creator, @creator, @creator].map do |_creator|
      create(:workgroup, creator_id: @creator.id)
    end
  end

  it 'gets the index' do
    path '/api/workgroups' do
      get(summary: 'Get workgroup index') do
        response(200, description: 'Get workgroup indexes') do
          body = JSON(response.body)
          expect(body.count).to eq(3)
        end
      end

      get(summary: 'test if archived not rendered') do
        before do
          create(:archived_workgroup)
        end

        response(200, description: 'Get workgroup indexes') do
          body = JSON(response.body)
          expect(body.count).to eq(3)
        end
      end
    end
  end

  it 'gets one workgroup' do
    path '/api/workgroups/{id}' do
      parameter :id, in: :path, type: :integer, required: true, description: 'Workgroup ID'

      response(200, description: 'Return the selected workgroup') do
        let(:id) { workgroups[0].id }
        it 'is the correct workgroup' do
          body = JSON(response.body)
          expect(body['id']).to eq(workgroups[0].id)
        end
      end

      response(404, description: 'Workgroup not found') do
        let(:id) { 999 }
      end
    end
  end
end

RSpec.describe 'WorkgroupApiProtected', type: :request do
  it 'creates a workgroup' do
    @user = create(:confirmed_user)

    path '/api/workgroups' do
      post(summary: 'Unauthorized') do
        parameter :data,
                  in: :body,
                  required: true

        response(401, description: 'Unauthorized') do
          let(:data) do
            {
              workgroup: {
                status: 'draft',
                title: 'A long Title',
                short_title: 'AshortTitle',
                logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                description: 'A description',
                short_description: 'A short description',
                is_private: false,
                interests: [1, 2],
                skills: ['Python', '3D printing']
              }
            }
          end
        end
      end

      sign_in @user

      path '/api/workgroups' do
        post(summary: 'Unauthorized') do
          parameter :data,
                    in: :body,
                    required: true

          response(201, description: 'Unauthorized') do
            let(:data) do
              {
                workgroup: {
                  status: 'draft',
                  title: 'A long Title',
                  short_title: 'AshortTitle',
                  logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                  description: 'A description',
                  short_description: 'A short description',
                  is_private: false,
                  interests: [1, 2],
                  skills: ['Python', '3D printing']
                }
              }
            end
            it 'has the right creator_id' do
              body = JSON(response.body)
              expect(body['creator_id']).to eq(@user.id)
            end
            it 'has the right data' do
              body = JSON(response.body)
              expect(body['title']).to eq('A long Title')
              expect(body['short_title']).to eq('AshortTitle')
              expect(body['logo_url']).to eq('https://robohash.org/voluptatumundesed.png?size=300x300')
              expect(body['description']).to eq('A description')
              expect(body['short_description']).to eq('A short description')
              expect(body['is_private']).to eq(false)
              expect(body['interests']).to eq([1, 2])
              expect(body['skills']).to eq(['Python', '3D printing'])
            end
          end
        end
      end
    end
  end

  it 'can destroy a workgroup' do
    @creator = create(:confirmed_user)
    @workgroup = create(:workgroup, creator_id: @creator.id)

    path '/api/workgroup/{id}' do
      delete
    end
  end

  it 'is unauthorized to update a workgroup' do
    @user = create(:user)
    @workgroup = create(:workgroup, creator_id: @user.id)

    path '/api/workgroups/{id}' do
      patch(summary: 'update a workgroup') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Workgroup ID'
        parameter :data,
                  in: :body,
                  required: true
        response(401, description: 'Unauthorized') do
          let(:data) do
            {
              workgroup: {
                status: 'draft',
                title: 'A long Title',
                short_title: 'AshortTitle',
                logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                description: 'A description',
                short_description: 'A short description',
                is_private: false,
                interests: [1, 2],
                skills: ['Python', '3D printing']
              }
            }
          end
        end
      end
    end
  end

  it 'is forbidden to update a workgroup' do
    @user = create(:user)
    @user2 = create(:user)
    @workgroup = create(:workgroup, creator_id: @user2.id)

    sign_in @user

    path '/api/workgroups/{id}' do
      patch(summary: 'Update a workgroup') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Workgroup ID'
        parameter :data,
                  in: :body,
                  required: true
        response(403, description: 'Unauthorized') do
          let(:data) do
            {
              workgroup: {
                status: 'draft',
                title: 'A long Title',
                short_title: 'AshortTitle',
                logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                description: 'A description',
                short_description: 'A short description',
                is_private: false,
                interests: [1, 2],
                skills: ['Python', '3D printing']
              }
            }
          end
        end
      end
    end
  end

  it 'creator can update workgroup' do
    @user = create(:confirmed_user)
    @workgroup = create(:workgroup, creator_id: @user.id)

    sign_in @user

    path '/api/workgroups/{id}' do
      patch(summary: 'Update a workgroup') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Workgroup ID'
        parameter :data,
                  in: :body,
                  required: true
        response(200, description: 'Success') do
          let(:data) do
            {
              workgroup: {
                status: 'draft',
                title: 'A long Title',
                short_title: 'AshortTitle',
                logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                description: 'A description',
                short_description: 'A short description',
                is_private: false,
                interests: [1, 2],
                skills: ['Python', '3D printing']
              }
            }
          end
          it 'is updated' do
            body = JSON(response.body)
            expect(body['title']).to eq('A long Title')
            expect(body['short_title']).to eq('AshortTitle')
            expect(body['logo_url']).to eq('https://robohash.org/voluptatumundesed.png?size=300x300')
            expect(body['description']).to eq('A description')
            expect(body['short_description']).to eq('A short description')
            expect(body['is_private']).to eq(false)
            expect(body['interests']).to eq([1, 2])
            expect(body['skills']).to eq(['Python', '3D printing'])
          end
        end
      end
    end
  end

  it 'admin can update workgroup' do
    @user = create(:confirmed_user)
    @admin = create(:confirmed_user)
    @workgroup = create(:workgroup, creator: @user)
    @admin.add_role(:admin, @workgroup)

    sign_in @admin

    path '/api/workgroups/{id}' do
      patch(summary: 'Update the workgroup') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Workgroup ID'
        parameter :data,
                  in: :body,
                  required: true
        response(200, description: 'Success') do
          let(:data) do
            {
              workgroup: {
                status: 'draft',
                title: 'A long Title',
                short_title: 'AshortTitle',
                logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                description: 'A description',
                short_description: 'A short description',
                is_private: false,
                interests: [1, 2],
                skills: ['Python', '3D printing']
              }
            }
          end
          it 'is updated' do
            body = JSON(response.body)
            expect(body['title']).to eq('A long Title')
            expect(body['short_title']).to eq('AshortTitle')
            expect(body['logo_url']).to eq('https://robohash.org/voluptatumundesed.png?size=300x300')
            expect(body['description']).to eq('A description')
            expect(body['short_description']).to eq('A short description')
            expect(body['is_private']).to eq(false)
            expect(body['interests']).to eq([1, 2])
            expect(body['skills']).to eq(['Python', '3D printing'])
          end
        end
      end
    end
  end

  it 'can join a workgroup' do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @workgroup = create(:workgroup, creator_id: @creator.id)

    sign_in @member

    path '/api/workgroup/{id}/join' do
      put(summary: 'Join workgroup') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Workgroup ID'

        response(200, description: 'Success')

        it 'has joined' do
          @workgroup.reload
          expect(@workgroup.users.include?(@member)).to be_truthy
          expect(UsersWorkgroup.where(workgroup_id: @workgroup.id, user_id: @member.id).first.role).to eq('member')
        end
      end
    end
  end

  it 'can leave a workgroup' do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @workgroup = create(:workgroup, creator_id: @creator.id)

    sign_in @member

    path '/api/workgroup/{id}/leave' do
      put(summary: 'Leave the workgroup') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Workgroup ID'

        response(200, description: 'Success')

        it 'has leaved' do
          @workgroup.reload
          expect(@workgroup.users.include?(@member)).to be_falsey
        end
      end
    end
  end

  it "can't join a private workgroup" do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @workgroup = create(:private_workgroup, creator_id: @creator.id)

    sign_in @member

    path '/api/workgroup/{id}/join' do
      put(summary: 'Join') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Workgroup ID'

        response(200, description: 'Success')

        it 'is pending' do
          @workgroup.reload
          expect(@workgroup.users.include?(@member)).to be_truthy
          expect(UsersWorkgroup.where(workgroup_id: @workgroup.id, user_id: @member.id).first.role).to eq('pending')
        end
      end
    end
  end

  it 'can invite a user' do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @workgroup = create(:private_workgroup, creator_id: @creator.id)

    sign_in @creator

    path '/api/workgroup/{id}/invite' do
      post(summary: 'Invite') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Workgroup ID'
        parameter :data,
                  in: :body,
                  required: true

        response(200, description: 'Success') do
          let(:data) do
            {
              user_id: @member.id
            }
          end
          it 'is a member' do
            @workgroup.reload
            expect(@workgroup.users.include?(@member)).to be_truthy
            expect(UsersWorkgroup.where(workgroup_id: @workgroup.id, user_id: @member.id).first.role).to eq('member')
          end
        end

        response(200, description: 'Success') do
          let(:data) do
            {
              stranger_email: 'test@test.com'
            }
          end
        end
      end
    end

    sign_out @creator

    # @workgroup.users.delete(@member)

    sign_in @member

    path '/api/workgroup/{id}/invite' do
      post(summary: 'Invite') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Workgroup ID'
        parameter :data,
                  in: :body,
                  required: true

        response(403, description: 'forbidden') do
          let(:data) do
            {
              user_id: @member.id
            }
          end
        end

        it 'is not a member' do
          @workgroup.reload
          expect(@workgroup.users.include?(@member)).to be_falsey
        end
      end
    end
  end
end
