# frozen_string_literal: true

require 'sinatra/base'

class FakeEventbrite < Sinatra::Base
  get '/v3/users/me/organizations/' do
    json_response 200, 'eventbrite/organizations.json'
  end

  get '/v3/organizations/:id/events/' do
    json_response 200, 'eventbrite/events.json'
  end

  private

  def json_response(response_code, file_name)
    content_type :json
    status response_code
    File.open(File.join(File.dirname(__FILE__), "../fixtures/#{file_name}"), 'rb').read
  end
end
