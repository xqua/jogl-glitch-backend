# frozen_string_literal: true

FactoryBot.define do
  factory :ckan_dataset, class: Datasets::CkanDataset do
    author
    url { 'https://data.gov.sg/dataset/rainfall-monthly-number-of-rain-days' }
  end
end
