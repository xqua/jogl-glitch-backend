# frozen_string_literal: true

FactoryBot.define do
  factory :mention do
    obj_match { '@user' }
  end
end
