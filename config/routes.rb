# frozen_string_literal: true

Rails.application.routes.draw do
  require 'sidekiq/cron/web'

  mount Coverband::Reporters::Web.new, at: '/coverage' if ENV['HEROKU_ENV'] == 'develop'
  mount Sidekiq::Web => '/sidekiq'

  devise_for :users,
             skip: [:passwords],
             path: 'api/auth',
             controllers: {
               confirmations: 'auth/confirmations',
               omniauth_callbacks: 'auth/omniauth_callbacks',
               sessions: 'auth/sessions'
             },
             defaults: {
               format: :json
             }

  get  'api/auth/password/token' => 'auth/passwords#token'
  post 'api/auth/password/reset' => 'auth/passwords#reset', as: :reset_password
  post 'api/auth/password/unknown' => 'auth/passwords#unknown', as: :unknown_password
  post 'api/auth/password/update' => 'auth/passwords#update', as: :update_password

  root 'api/pages#index'

  post 'graphql', to: 'graphql#execute'

  resources :external_links

  namespace :api do
    get '/auth/validate_token', to: 'users#validate_token'

    resources :admin, except: %i[index show create update destroy] do
      collection do
        get '/moderator' => 'admin#moderator'
        post '/users_with_skills', to: 'admin#users_with_skills', defaults: { format: 'json' }
        post '/email_users_with_skills', to: 'admin#email_users_with_skills'
      end
    end

    # CONCERNS #

    concern :affiliatable do
      collection do
        get '/:id/affiliations' => :affiliation_index
        get '/:id/affiliations/:parent_type/:parent_id' => :affiliation_show
        post '/:id/affiliations/:parent_type/:parent_id' => :affiliation_create
        patch '/:id/affiliations/:parent_type/:parent_id' => :affiliation_update
        delete '/:id/affiliations/:parent_type/:parent_id' => :affiliation_destroy
      end
    end

    concern :affiliatedable do
      collection do
        get '/:id/affiliated/stats' => :affiliated_stats
        get '/:id/affiliated/:affiliate_type' => :affiliated_index
        post '/:id/affiliated/:affiliate_type/:affiliate_id' => :affiliated_create
      end
    end

    concern :avatarable do
      collection do
        post '/:id/avatar' => :upload_avatar
        delete '/:id/avatar' => :remove_avatar
      end
    end

    concern :bannerable do
      collection do
        post '/:id/banner' => :upload_banner
        delete '/:id/banner' => :remove_banner
      end
    end

    concern :clapable do
      collection do
        put    '/:id/clap'             => :clap
        get    '/:id/clap'             => :clap
        delete '/:id/clap'             => :clap
        get    '/:id/clappers'         => :clappers
      end
    end

    concern :customdatable do
      collection do
        post   '/:id/customfield'                     => :create_custom_field
        patch  '/:id/customfield/:field_id'           => :update_custom_field
        delete '/:id/customfield/:field_id'           => :delete_custom_field
        get    '/:id/customfield'                     => :get_custom_fields
        post   '/:id/customdata/:field_id'            => :create_custom_data
        patch  '/:id/customdata/:field_id'            => :update_custom_data
        get    '/:id/customdata/:field_id'            => :get_custom_data
        get    '/:id/customdata'                      => :get_my_custom_datas
      end
    end

    concern :documentable do
      collection do
        post    '/:id/documents'               => :upload_document
        delete  '/:id/documents/:document_id'  => :remove_document
      end
    end

    concern :faqable do
      collection do
        post '/:id/faq' => :faq_create
        get '/:id/faq' => :faq_index
        patch '/:id/faq/:document_id' => :faq_update
        get '/:id/faq/:document_id' => :faq_show
        delete '/:id/faq/:document_id' => :faq_destroy
      end
    end

    concern :followable do
      collection do
        put    '/:id/follow'           => :follow
        get    '/:id/follow'           => :follow
        delete '/:id/follow' => :follow
        get    '/:id/followers' => :followers
      end
    end

    concern :hookable do
      collection do
        post   '/:id/hooks'                     => :create_external_hook
        patch  '/:id/hooks/:hook_id'            => :update_external_hook
        delete '/:id/hooks/:hook_id'            => :delete_external_hook
        get    '/:id/hooks'                     => :get_external_hooks
      end
    end

    concern :joinable do
      collection do
        put   '/:id/join'           => :join
        put   '/:id/leave'          => :leave
      end
    end

    concern :linkable do
      collection do
        post '/:id/links' => :create_link
        get '/:id/links' => :index_link
        patch '/:id/links/:link_id' => :update_link
        delete '/:id/links/:link_id' => :destroy_link
      end
    end

    concern :mediumable do
      collection do
        post '/:id/media' => :media_create
        get '/:id/media' => :media_index
        patch '/:id/media/:medium_id' => :media_update
        put '/:id/media/:medium_id' => :media_upload
        get '/:id/media/:medium_id' => :media_show
        delete '/:id/media/:medium_id' => :media_destroy
      end
    end

    concern :memberable do
      collection do
        post '/:id/invite' => :invite
        post '/:id/invite_as_admin' => :invite_as_admin
        post '/:id/is_member' => :has_membership
        get '/:id/members' => :members_list
        post '/:id/members' => :update_member
        delete '/:id/members/:user_id' => :remove_member
      end
    end

    concern :recommendable do
      collection do
        get '/recommended' => :recommended
        get '/:id/similar' => :similar
      end
    end

    concern :resourceable do
      collection do
        get    '/:id/resources'              => :index_resource
        post   '/:id/resources'              => :add_resource
        patch  '/:id/resources/:resource_id' => :update_resource
        delete '/:id/resources/:resource_id' => :remove_resource
      end
    end

    concern :reviewable do
      collection do
        put '/:id/review'    => :review
        get '/:id/review'    => :review
        delete '/:id/review' => :review
      end
    end

    concern :saveable do
      collection do
        put    '/:id/save' => :save
        get    '/:id/save' => :save
        delete '/:id/save' => :save
      end
    end

    concern :serviceable do
      collection do
        get '/:id/services'   => :index_service
        get '/:id/activities' => :index_activity
      end
    end

    concern :short_titleable do
      collection do
        get  '/exists/:short_title'         => :short_title_exist
        get  '/getid/:short_title'          => :get_id_from_short_title
      end
    end

    # RESOURCES #

    ## PEER REVIEW BEGIN ##
    resources :answers, except: [:index]

    post 'peer_reviews/:id/email-admins'        => 'peer_reviews#email_admins'
    post 'peer_reviews/:id/email-admins-single' => 'peer_reviews#email_admins_single'
    post 'peer_reviews/:id/email-reviewers'     => 'peer_reviews#email_reviewers'
    post 'peer_reviews/:id/email-users'         => 'peer_reviews#email_users'
    post 'peer_reviews/:id/get-admins-info'     => 'peer_reviews#get_admins_info', :defaults => { :format => 'json' }
    post 'peer_reviews/:id/get-reviewers-info'  => 'peer_reviews#get_reviewers_info', :defaults => { :format => 'json' }
    get  'peer_reviews/:id/people'              => 'peer_reviews#index_people'
    get  'peer_reviews/:id/proposals'           => 'peer_reviews#index_proposals'
    delete 'peer_reviews/:id/proposals/:proposal_id'   => 'peer_reviews#remove_proposal'
    get  'peer_reviews/mine'                    => 'peer_reviews#my_peer_reviews'

    resources :peer_reviews, concerns: %i[bannerable faqable followable linkable joinable memberable saveable short_titleable]

    resources :programs, only: [] do
      resources :peer_reviews, only: [:index]
    end

    get 'proposals/mine'        => 'proposals#my_proposals'
    get 'proposals/:id/answers' => 'proposals#index_answers'

    resources :proposals, except: [:index], concerns: %i[joinable memberable resourceable saveable]

    resources :spaces, only: [] do
      resources :peer_reviews, only: [:index]
    end

    ## PEER REVIEW END ##

    resources :algolium, only: [:create], defaults: { format: :json }

    resources :boards, except: %i[create destroy index show update] do
      collection do
        post   '/:id/users/:user_id' => :add_user
        delete '/:id/users/:user_id' => :remove_user
      end
    end

    resources :challenges, concerns: %i[affiliatable avatarable bannerable
                                        documentable faqable followable hookable
                                        joinable linkable mediumable memberable
                                        recommendable saveable short_titleable] do
      collection do
        get    '/:id/projects'               => :index_projects
        get    '/:id/needs'                  => :index_needs
        post   '/:id/projects/:project_id'   => :set_project_status
        put    '/:id/projects/:project_id'   => :attach
        delete '/:id/projects/:project_id'   => :remove
        get    '/:id/members_short'          => :members_short
        get    '/mine'                       => :my_challenges
        get    '/can_create'                 => :can_create
      end
    end

    resources :datasets, concerns: %i[followable mediumable recommendable saveable] do
      collection do
        post '/make' => :make
      end
    end

    resources :feeds, except: %i[create destroy] do
      collection do
        get      '/all'                      => :indexall
        delete   '/:id/:post_id'             => :remove_post
      end
    end

    resources :needs, concerns: %i[documentable followable joinable
                                   mediumable memberable
                                   recommendable saveable] do
      collection do
        get '/mine' => :my_needs
      end
    end

    resources :networks, only: %i[index show]

    resources :notifications, only: %i[index show] do
      collection do
        put   '/:id/read'                        => :read
        put   '/:id/unread'                      => :unread
        put   '/readall'                         => :all_read
        put   '/unreadall'                       => :all_unread
        get   '/settings'                        => :settings
        post  '/settings'                        => :set_settings
      end
    end

    resources :posts, except: :index, concerns: %i[clapable documentable recommendable saveable] do
      collection do
        post   '/:id/comment'                => :comment
        patch  '/:id/comment/:comment_id'    => :comment
        delete '/:id/comment/:comment_id' => :comment
      end
    end

    resources :projects, concerns: %i[affiliatable bannerable customdatable
                                      documentable followable hookable
                                      joinable linkable mediumable memberable
                                      recommendable reviewable
                                      saveable short_titleable] do
      collection do
        get '/mine'                      => :my_projects
        get '/:id/needs'                 => :index_needs
      end
    end

    resources :programs, concerns: %i[affiliatable avatarable bannerable documentable
                                      faqable followable hookable joinable
                                      linkable mediumable memberable recommendable
                                      resourceable saveable short_titleable] do
      resources :boards do
        collection do
          post    '/:id/users/:user_id'         => :add_user
          delete  '/:id/users/:user_id'         => :remove_user
        end
      end

      collection do
        get    '/:id/documents'                  => :documents
        get    '/:id/members_short'              => :members_short
        get    '/:id/needs'                      => :index_needs
        get    '/:id/projects'                   => :index_projects
        get    '/:id/challenges'                 => :index_challenges
        put    '/:id/challenges/:challenge_id'   => :attach
        delete '/:id/challenges/:challenge_id'   => :remove
        get    '/mine'                           => :my_programs
        get    '/can_create'                     => :can_create
      end
    end

    resources :services, only: [] do
      collection do
        get  '/:id'             => :show
        get  '/:id/flows'       => :index_flows
        put  '/:id/flows/:type' => :fetch_service_flow_data
        get  '/:id/flows/:type' => :load_service_flow_data
        post '/:id/flows/:type' => :copy_service_flow_data
        delete '/:id/activities/:activity_id' => :delete_activity
      end
    end

    resources :skills, only: [:index]

    resources :spaces, concerns: %i[affiliatedable avatarable bannerable documentable
                                    faqable followable hookable joinable linkable mediumable
                                    memberable resourceable saveable serviceable short_titleable] do
      resources :boards do
        collection do
          post    '/:id/users/:user_id'         => :add_user
          delete  '/:id/users/:user_id'         => :remove_user
        end
      end

      member do
        get 'affiliates'
      end

      collection do
        get    '/:id/needs'                      => :index_needs
        get    '/:id/projects'                   => :index_projects
        get    '/:id/challenges'                 => :index_challenges
        get    '/:id/people'                     => :index_people
        delete '/:id/challenges/:challenge_id'   => :remove
        get    '/mine'                           => :my_spaces
        get    '/can_create'                     => :can_create
        post   '/:id/email-members'              => :email_members
      end
    end
    
    # post 'spaces/:id/get-members-info'  => 'spaces#get_members_info', :defaults => { :format => 'json' }

    resources :users, except: [:destroy], concerns: %i[affiliatable avatarable followable
                                                       linkable recommendable saveable] do
      collection do
        delete '/archive'                  => :archive
        delete '/:id'                      => :destroy
        get    '/exists/:nickname'         => :nickname_exist
        post   '/resend_confirmation'      => :resend_confirmation
        get    '/saved_objects'            => :saved_objects
        get    '/:id/mutual'               => :mutual
        get    '/:id/following'            => :following
        get    '/:id/objects/:object_type' => :user_object
        post   '/:id/send_email'           => :send_private_email
      end
    end

    resources :workgroups, concerns: %i[affiliatable bannerable documentable
                                        followable hookable joinable linkable
                                        mediumable memberable recommendable
                                        saveable short_titleable] do
      collection do
        get '/mine' => :my_workgroups
      end
    end
  end
end
