# frozen_string_literal: true

# Use this hook to configure devise mailer, warden hooks and so forth.
# Many of these configuration options can be set straight in your model.
Devise.setup do |config|
  config.secret_key = '860fd21fcfc0dcf84f2d59677a85eeafd62169096d554d6111fb904dd7ab7209a8041587b09bfd9818b59861d91be01ff22d40b52bf85c977529c787ad2fbf2f'

  config.navigational_formats = []

  config.mailer_sender = ENV['JOGL_NOTIFICATIONS_EMAIL']

  config.mailer = 'DeviseMailer'

  require 'devise/orm/active_record'

  config.case_insensitive_keys = [:email]

  config.strip_whitespace_keys = [:email]

  config.stretches = Rails.env.test? ? 1 : 11

  config.reconfirmable = true

  config.remember_for = 2.weeks

  config.expire_all_remember_me_on_sign_out = true

  config.password_length = 6..128

  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/

  config.lock_strategy = :none

  config.unlock_keys = [:email]

  config.maximum_attempts = 5

  config.last_attempt_warning = true

  config.reset_password_within = 6.hours

  config.sign_out_via = :delete

  config.omniauth :eventbrite, ENV['EVENTBRITE_CLIENT_ID'], ENV['EVENTBRITE_CLIENT_SECRET']

  config.jwt do |jwt|
    jwt.secret = ENV['DEVISE_JWT_SECRET_KEY']
    jwt.expiration_time = 2_592_000
  end

  config.skip_session_storage = %i[http_auth params_auth]
end
