# frozen_string_literal: true

namespace :data_migration do
  desc 'transform user age integers into approximate birth year'
  task age_to_birth_year: :environment do
    User.where(birth_date: nil).where.not(age: nil).find_each do |user|
      # we only use bith_year for now, but we store it as a date,
      # so month and day are set to Jan 1 for all users
      birth_day = user.age.years.ago.beginning_of_year.to_date
      user.update_column(:birth_date, user.age.years.ago.beginning_of_year.to_date)
      message = "updating user #{user.id} birth_date to: #{birth_day} (using #{user.age})"
      Rails.logger.info(message)
      puts message
    end
  end
end
