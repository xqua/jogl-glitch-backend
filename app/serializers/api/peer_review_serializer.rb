# frozen_string_literal: true

module Api
  class PeerReviewSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::RolesSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :id,
               :banner_url,
               :banner_url_sm,
               :created_at,
               :deadline,
               :feed_id,
               :followers_count,
               :interests,
               :is_member_of_parent,
               :maximum_disbursement,
               :options,
               :proposals_count,
               :resource_id,
               :resource_type,
               :saves_count,
               :score_threshold,
               :short_title,
               :skills,
               :start,
               :stop,
               :submitted_at,
               :summary,
               :template_question_count,
               :title,
               :total_funds,
               :updated_at,
               :visibility

    attribute :has_followed, unless: :scope?
    attribute :has_saved, unless: :scope?
    attribute :is_admin, unless: :scope?
    attribute :is_member, unless: :scope?
    attribute :description, if: :controller?
    attribute :is_member_of_parent, if: :controller?
    attribute :proposals, if: :controller?
    attribute :rules, if: :controller?

    def controller?
      @instance_options[:controller]
    end

    def proposals_count
      object.proposals.count
    end

    def template_question_count
      object.faq.documents.count
    end

    def is_member_of_parent
      current_user&.has_role?(:member, object.resource)
    end

    def proposals
      object.proposals.map do |proposal|
        Api::ProposalSerializer.new(proposal)
      end
    end
  end
end
