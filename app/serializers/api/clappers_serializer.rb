# frozen_string_literal: true

module Api
  class ClappersSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper

    attributes :clappers

    def clappers
      includes = %i[user]

      object
        .relations
        .clapped
        .includes(includes)
        .map do |relation|
          user = relation.user
          user = Users::DeletedUser.new unless user.active?
          data = {
            id: user.id,
            first_name: user.first_name,
            last_name: user.last_name,
            nickname: user.nickname,
            logo_url: user.logo_url,
            logo_url_sm: user.logo_url_sm,
            has_followed: has_followed(user),
            has_clapped: has_clapped(user),
            short_bio: user.short_bio
          }
          if object.instance_of?(Post)
            data = data.merge({
                                can_contact: user.can_contact,
                                projects_count: user.projects_count
                              })
          end
          data
        end
    end
  end
end
