# frozen_string_literal: true

module Api
  class ProposalSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::RolesSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :id,
               :created_at,
               :funding,
               :interests,
               :peer_review_id,
               :project_id,
               :score,
               :short_title,
               :skills,
               :submitted_at,
               :summary,
               :title,
               :updated_at

    attribute :is_admin, unless: :scope?
    attribute :is_member, unless: :scope?
    attribute :is_peer_review_admin, if: :controller?
    attribute :is_reviewer, unless: :scope?
    attribute :is_validated

    # has_many :answers

    def controller?
      @instance_options[:controller]
    end

    def is_peer_review_admin
      current_user&.has_role?(:admin, object.peer_review)
    end

    def is_validated
      object.score && object.peer_review.score_threshold && object.score >= object.peer_review.score_threshold
    end
  end
end
