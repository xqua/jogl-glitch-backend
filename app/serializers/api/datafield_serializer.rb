# frozen_string_literal: true

module Api
  class DatafieldSerializer < ActiveModel::Serializer
    attributes :id,
               :name,
               :title,
               :description,
               :format,
               :unit,
               :created_at,
               :updated_at
  end
end
