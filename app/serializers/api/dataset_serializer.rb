# frozen_string_literal: true

module Api
  class DatasetSerializer < ActiveModel::Serializer
    attributes :id,
               :title,
               :short_title,
               :url,
               :description,
               :type,
               :author_id,
               :provider,
               :provider_id,
               :provider_type,
               :provider_url,
               :provider_author,
               :provider_authoremail,
               :provider_version,
               :provider_created_at,
               :provider_updated_at

    has_many :data
    has_many :sources
    has_many :tags

    has_one :license
  end
end
