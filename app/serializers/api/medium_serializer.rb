# frozen_string_literal: true

module Api
  class MediumSerializer < ActiveModel::Serializer
    attributes :id,
               :title,
               :alt_text,
               :item_url,
               :item_url_sm
  end
end
