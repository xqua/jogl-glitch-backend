# frozen_string_literal: true

module Api
  class DocumentSerializer < ActiveModel::Serializer
    attributes :id,
               :content,
               :position,
               :title
  end
end
