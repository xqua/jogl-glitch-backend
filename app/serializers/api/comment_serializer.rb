# frozen_string_literal: true

module Api
  class CommentSerializer < ActiveModel::Serializer
    include Api::UsersSerializerHelper

    attributes :id,
               :content,
               :creator,
               :created_at
  end
end
