# frozen_string_literal: true

module Api
  class ExternalLinkSerializer < ActiveModel::Serializer
    attributes :id,
               :url,
               :name,
               :icon_url
  end
end
