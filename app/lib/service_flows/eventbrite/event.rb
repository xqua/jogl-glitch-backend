# frozen_string_literal: true

require_relative '../../service_flow.rb'

module ServiceFlows::Eventbrite
  class Event < ServiceFlow
    def fetch_data
      puts "GET DATA FOR SERVICE #{@service.name} FLOW #{flow}"
    end

    def copy_data(_opts)
      puts "COPY DATA FOR SERVICE #{@service.name} FLOW #{flow}"
    end
  end
end
