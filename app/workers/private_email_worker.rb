# frozen_string_literal: true

class PrivateEmailWorker
  include Sidekiq::Worker

  def perform(from_id, to_id, object, content)
    from = User.find(from_id)
    to = User.find(to_id)

    PrivateMailer.send_private_email(from, to, object, content).deliver unless to.deleted?
  end
end
