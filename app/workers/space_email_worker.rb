# frozen_string_literal: true

class SpaceEmailWorker
  include Sidekiq::Worker

  def perform(from_id, to_id, space_id, content, subject)
    from = User.find(from_id)
    to = User.find(to_id)
    space = Space.find(space_id)

    SpaceMailer.send_private_email(from, to, space, content, subject).deliver unless to.deleted?
  end
end
