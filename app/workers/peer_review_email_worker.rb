# frozen_string_literal: true

class PeerReviewEmailWorker
  include Sidekiq::Worker

  def perform(from_id, to_id, peer_review_id, content, subject)
    from = User.find(from_id)
    to = User.find(to_id)
    peer_review = PeerReview.find(peer_review_id)

    PeerReviewMailer.send_private_email(from, to, peer_review, content, subject).deliver unless to.deleted?
  end
end
