# frozen_string_literal: true

class ContentSizeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return unless value.attached?
    return if value.byte_size < content_size

    value.purge
    record.errors.add(attribute, :byte_size, options)
  end

  private

  def byte_size
    options.fetch(:in)
  end
end
