# frozen_string_literal: true

class Answer < ApplicationRecord
  belongs_to :document
  belongs_to :proposal
end
