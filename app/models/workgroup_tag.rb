# frozen_string_literal: true

class WorkgroupTag < ApplicationRecord
  has_and_belongs_to_many :workgroups
end
