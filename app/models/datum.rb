# frozen_string_literal: true

class Datum < ApplicationRecord
  belongs_to :author, foreign_key: 'author_id', class_name: 'User'
  belongs_to :dataset

  has_many :datafields
  has_many :sources, as: :sourceable

  has_one :license, as: :licenseable

  has_one_attached :document

  has_paper_trail
end
