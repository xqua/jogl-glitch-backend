# frozen_string_literal: true

class Tag < ApplicationRecord
  include RecsysHelpers

  belongs_to :tagable, polymorphic: true
end
