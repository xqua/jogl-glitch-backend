# frozen_string_literal: true

# Application of a Project to a PeerReview.
class Question < ApplicationRecord
  ## FIELDS ##
  belongs_to :peer_review

  has_many :answers, dependent: :destroy

  ## VALIDATIONS ##
  validates :peer_review, presence: true
end
