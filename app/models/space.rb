# frozen_string_literal: true

# Umbrella object that holds this hierarchy: spaces -> program -> challenges -> projects -> needs
# Users can become affiliated to the space

class Space < ApplicationRecord
  include AffiliatableParent
  include AlgoliaSearch
  include AttrJson::Record
  include Avatarable
  include Bannerable
  include Boardable
  include Feedable
  # include Interestable
  include Linkable
  include Mediumable
  include Membership
  include NotificationsHelpers
  include RecsysHelpers
  include RelationHelpers
  include Serviceable
  # include Skillable
  include Utils

  belongs_to :creator, foreign_key: 'creator_id', class_name: 'User'

  enum status: %i[draft soon active completed archived]
  enum space_type: {  digital_community: 0,
                      local_community: 1,
                      ngo: 2,
                      not_for_profit_organization: 3,
                      startup: 4,
                      maker_space: 5,
                      community_lab: 6,
                      company: 7,
                      social_enterprise: 8,
                      school: 9,
                      foundation: 10,
                      academic_lab: 11,
                      professional_network: 12,
                      public_agency: 13,
                      public_institution: 14,
                      incubator: 15,
                      living_lab: 16,
                      fund: 17,
                      other: 18 }

  has_many :challenges, after_add: %i[reindex], after_remove: %i[reindex]
  has_many :peer_reviews, as: :resource
  has_many :projects, after_add: %i[reindex], after_remove: %i[reindex]
  has_many :resources, as: :documentable, class_name: 'Document', dependent: :destroy
  has_many :workgroups, after_add: %i[reindex], after_remove: %i[reindex]

  has_many_attached :documents

  has_one :faq, as: :faqable, dependent: :destroy

  before_create :sanitize_description
  before_update :sanitize_description

  validates :short_title, uniqueness: true
  validates :title, presence: true

  notification_object
  resourcify
  valid_affiliates :challenge, :program, :project, :user

  attr_json :available_tabs, :json, default:
    {
      programs: true,
      challenges: true,
      resources: true,
      enablers: true,
      faqs: true
    }

  attr_json :selected_tabs, :json, default:
    {
      programs: true,
      challenges: true,
      resources: true,
      enablers: true,
      faqs: true
    }

  attr_json :show_featured, :json, default: {
    challenges: true,
    needs: true,
    programs: true,
    projects: true
  }

  algoliasearch disable_indexing: !Rails.env.production?, unless: :draft? do
    use_serializer Api::SpaceSerializer
  end

  def needs_count
    (Need.where(project_id: Affiliation.where(parent: self, affiliate_type: 'Project', status: 'accepted').pluck(:affiliate_id)) + Need.where(project_id: ::Project.select("projects.*, 'associated' AS relation").joins(:challenges).where(challenges: { space: self }).pluck(:id))).uniq(&:id).count
  end

  def projects_affiliated
    Project.where(id: Affiliation.where(parent: self, affiliate_type: 'Project', status: 'accepted').pluck(:affiliate_id)).where.not(status: "draft")
  end

  def projects_associated
    Project.joins(:challenges).where(challenges: { space: self })
  end
  
  def members
    User.with_role(:member, self)
    # User.select("users.*, 'member' as relation").joins('LEFT JOIN users_roles ON users.id = users_roles.user_id').joins('LEFT JOIN roles ON roles.id = users_roles.role_id').where("roles.name != 'pending' AND roles.resource_type = 'Space' AND roles.resource_id = :space_id", space_id: self.id)
  end

  def challenge_participants
    User.select("users.*, 'participant' as relation").joins('LEFT JOIN users_roles ON users.id = users_roles.user_id').joins('LEFT JOIN roles ON roles.id = users_roles.role_id').where("roles.name != 'pending' AND roles.resource_type = 'Challenge' AND roles.resource_id IN (SELECT id FROM challenges WHERE space_id = :space_id)", space_id: self.id)
  end

  def project_participants
    User.select("users.*, 'participant' as relation").joins('LEFT JOIN users_roles ON users.id = users_roles.user_id').joins('LEFT JOIN roles ON roles.id = users_roles.role_id').where("roles.name != 'pending' AND roles.resource_type = 'Project' AND roles.resource_id IN (SELECT id FROM projects WHERE space_id = :space_id)", space_id: self.id)
  end

  def participants
    self.show_associated_users? ? challenge_participants + project_participants : []
  end

  # change projects_count depending on if show_associated_projects is true or false
  def projects_count
    self.show_associated_projects? ? ((projects_affiliated + projects_associated).uniq).count : projects_affiliated.count
  end

  # change members_count depending on if show_associated_users is true or false
  def members_count
    self.show_associated_users? ? ((members + participants).uniq).count : members.uniq.count
  end

  def challenges_count
    challenges.accepted_challenge.count
  end

  def activities_count
    activities.count + peer_reviews.count
  end

  def frontend_link
    "/space/#{short_title}"
  end

  def notif_pending_join_request(requestor)
    Notification.create(
      target: requestor,
      category: :membership,
      type: 'pending_join_request',
      object: self
    )
  end

  def notif_pending_join_request_approved(requestor)
    Notification.create(
      target: requestor,
      category: :membership,
      type: 'pending_join_request_approved',
      object: self
    )
  end

  def all_owners_admins_members
    owners
      .or(admins)
      .or(members)
      .active
      .order(Arel.sql("users.id, roles.name='owner' DESC, roles.name='admin' DESC, roles.name='member' DESC"))
      .except(:select)
      .select('DISTINCT ON (users.id) users.*, roles.name AS role')
  end

  private

  def default_banner_url
    ActionController::Base.helpers.image_url('default-program.jpg')
  end

  def default_avatar_url
    ActionController::Base.helpers.image_url('default-avatar.png')
  end
end
