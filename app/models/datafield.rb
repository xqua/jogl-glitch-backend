# frozen_string_literal: true

class Datafield < ApplicationRecord
  belongs_to :datum
end
