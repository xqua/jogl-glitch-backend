# frozen_string_literal: true

module Api
  class ProposalsController < ApplicationController
    before_action :authenticate_user!, only: %i[create update destroy my_proposals]
    before_action :load_proposal, only: %i[destroy index_answers invite leave members_list remove_member show update update_member]
    before_action :is_admin, only: %i[destroy invite remove_member update update_member]

    include Api::Members
    include Api::Relations
    include Api::Resources
    include Api::Utils

    def create
      proposal = Proposal.new(proposal_params)

      if proposal.save
        proposal.update_interests(params[:proposal][:interests]) unless params[:proposal][:interests].nil?
        proposal.update_skills(params[:proposal][:skills]) unless params[:proposal][:skills].nil?

        current_user.add_role :owner, proposal
        current_user.add_role :admin, proposal
        current_user.add_role :member, proposal

        render json: proposal, status: :created
      else
        render json: proposal.errors.full_messages, status: :unprocessable_entity
      end
    end

    def destroy
      render status: :method_not_allowed if @proposal.submitted?

      @proposal.destroy

      render status: :ok
    end

    def index_answers
      answers = Answer
                .where(proposal_id: @proposal.id)
                .order(id: :desc)

      @pagy, records = pagy(answers)
      render json: records, each_serializer: Api::AnswerSerializer, root: 'answers', adapter: :json
    end

    def my_proposals
      proposals = Proposal.with_role(:owner, current_user)
      proposals += Proposal.with_role(:admin, current_user)
      proposals += Proposal.with_role(:member, current_user)

      render json: proposals.uniq.sort_by(&:updated_at)
    end

    def show
      proposal = Proposal.find(params[:id])

      render json: proposal, status: :ok, controller: true
    end

    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/CyclomaticComplexity
    # rubocop:disable Metrics/PerceivedComplexity
    def update
      project = Project.find(@proposal.project_id)

      if proposal_params[:score] && @proposal.score && @proposal.peer_review.score_threshold
        if @proposal.score > @proposal.peer_review.score_threshold
          User.with_role(:reviewer).first.add_role(:reviewer, project)
        else
          User.with_role(:reviewer).first.remove_role(:reviewer, project)
        end
      end

      if @proposal.update(proposal_params)
        @proposal.update_interests(params[:proposal][:interests]) unless params[:proposal][:interests].nil?
        @proposal.update_skills(params[:proposal][:skills]) unless params[:proposal][:skills].nil?

        render json: @proposal.reload, status: :ok
      else
        render json: @proposal.errors.full_messages, status: :unprocessable_entity
      end
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/CyclomaticComplexity
    # rubocop:enable Metrics/PerceivedComplexity

    private

    def proposal_params
      if @proposal && current_user.has_role?(:admin, @proposal.peer_review)
        # make it so that admin can edit everything + score (which normal members can't change)
        params.require(:proposal).permit(:funding, :peer_review_id, :project_id, :short_title, :submitted_at, :summary, :title, :score)
      else
        params.require(:proposal).permit(:funding, :peer_review_id, :project_id, :short_title, :submitted_at, :summary, :title)
      end
    end

    def is_admin
      render status: :forbidden unless @proposal &&
                                       (current_user.has_role?(:admin, @proposal) ||
                                        current_user.has_role?(:admin, @proposal.peer_review))
    end

    def load_proposal
      @proposal = Proposal.find(params.require(:id))
      @obj = @proposal

      render status: :not_found if @proposal.nil?
    end
  end
end
