# frozen_string_literal: true

module Api
  class AdminController < ApplicationController
    before_action :authenticate_user!
    before_action :is_moderator!, only: %i[users_with_skills email_users_with_skills]

    def moderator
      render json: { moderator: current_user.has_role?(:moderator) }, status: :ok
    end

    def users_with_skills
      respond_to do |format|
        format.csv do
          send_data User.to_csv(users_skills), filename: "users-with-skills-#{Time.zone.today}.csv"
        end

        format.json do
          render json: {
            count: users_skills.length,
            users: users_skills.map { |user| Api::UserSerializer.new(user) }
          }, status: :ok
        end
      end
    end

    def email_users_with_skills
      params.require(%i[content object skills])

      users_skills.pluck(:id).each do |user_id|
        PrivateEmailWorker.perform_async(current_user.id, user_id, params[:object], params[:content])
      end

      render json: { message: 'Emailing started' }, status: :ok
    end

    private

    def users_skills
      params.require(:skills)

      User.joins(:skills).where(skills: { id: params[:skills] }).uniq
    end

    def is_moderator!
      render status: :forbidden and return unless current_user.has_role?(:moderator)
    end
  end
end
