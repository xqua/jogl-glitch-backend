# frozen_string_literal: true

module Auth
  class PasswordsController < ApplicationController
    before_action :authenticate_user!, except: %i[unknown update]

    def token
      raw, enc = Devise.token_generator.generate(User, :reset_password_token)

      current_user.reset_password_token = enc
      current_user.reset_password_sent_at = Time.now.utc

      current_user.save(validate: false)

      render json: { token: raw }, status: :ok
    end

    def reset
      current_user.send_reset_password_instructions

      render status: :ok
    end

    def unknown
      params.require(:email)

      user = User.find_by(email: params[:email])

      render status: :ok and return if user.nil?

      user.send_reset_password_instructions

      render status: :ok
    end

    def update
      params.require(%i[reset_password_token password password_confirmation])

      user = User.reset_password_by_token(params)

      render status: :ok and return if user.errors.empty?

      render json: user.errors, status: :unprocessable_entity
    end
  end
end
