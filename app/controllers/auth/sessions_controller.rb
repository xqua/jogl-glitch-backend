# frozen_string_literal: true

module Auth
  # Extend devise in order to unwrap user parameters.
  class SessionsController < Devise::SessionsController
    wrap_parameters :user, format: [:json]

    # Create a user session.
    def create
      user = User.find_by(email: sign_in_params[:email])

      if user&.valid_password?(sign_in_params[:password])
        @current_user = user
        render json: user, status: :ok
      else
        render json: { errors: { 'email or password' => ['is invalid'] } }, status: :unprocessable_entity
      end
    end
  end
end
