# frozen_string_literal: true

module Api
  module Affiliation
    extend ActiveSupport::Concern

    included do
      before_action :affiliation_authenticate_user!, only: %i[affiliation_create affiliation_update affiliation_destroy]
      before_action :find_parent, only: %i[affiliation_show affiliation_create affiliation_update affiliation_destroy]
      before_action :is_parent_admin, only: %i[affiliation_update]
      before_action :is_obj_admin, only: %i[affiliation_create affiliation_destroy]
      before_action :is_self, only: %i[affiliation_create affiliation_destroy]
      before_action :set_affiliation, only: %i[affiliation_show affiliation_update affiliation_destroy]
    end

    def affiliation_index
      affiliations = @obj.affiliations.preload(:parent)
      response = affiliations.map do |affiliation|
        {
          parent_type: affiliation.parent_type,
          parent_id: affiliation.parent_id,
          status: affiliation.status,
          title: affiliation.parent.title,
          short_title: affiliation.parent.short_title,
          banner_url: affiliation.parent.banner_url,
          banner_url_sm: affiliation.parent.banner_url_sm,
          logo_url_sm: affiliation.parent.logo_url_sm
        }
      end
      render json: { parents: response }, status: :ok
    end

    def affiliation_show
      render json: @affiliation, status: :ok
    end

    def affiliation_create
      render json: { error: 'Object not set' }, status: :not_found and return if @obj.nil?

      affiliation = @obj.add_affiliation_to(@parent)

      render json: { error: 'Affiliation could not be created' }, status: :unprocessable_entity and return unless affiliation

      render json: { success: true }, status: :created
    end

    def affiliation_update
      @affiliation.status = params[:affiliations][:status]
      @affiliation.save

      render json: { error: 'Affiliation could not be updated' }, status: :unprocessable_entity and return unless @affiliation

      render json: { success: true }, status: :ok
    end

    def affiliation_destroy
      @obj.remove_affiliation_to(@parent)
      if ::Affiliation.find_by(affiliate: @obj, parent: @parent).nil?
        render json: { success: true }, status: :ok
      else
        render json: { errors: ["Error while deleting the affiliation"] }
      end
    end

    private

    def set_affiliation
      render json: { error: 'Object not set' }, status: :not_found and return unless @obj

      @affiliation = ::Affiliation.find_by(affiliate: @obj, parent: @parent)

      render json: { error: 'Affiliation not found' }, status: :not_found and return unless @affiliation
    end

    def is_parent_admin
      unless current_user.has_role?(:admin, @parent) || current_user.has_role?(:admin)
        render json: { error: "Only parents admin can update affiliations" }, status: :forbidden and return
      end
    end

    def is_obj_admin
      if @obj.class.name != "User"
        unless current_user.has_role?(:admin, @obj) || current_user.has_role?(:admin, @parent) || current_user.has_role?(:admin)
          render json: { error: "Only object admin can declare affiliations" }, status: :forbidden and return
        end
      end
    end

    def is_self
      if @obj.class.name == "User"
        unless current_user.id == @obj.id
          render json: { error: "Only object admin can declare affiliations" }, status: :forbidden and return
        end
      end
    end

    def find_parent
      params.require([:parent_type, :parent_id])

      klass = params[:parent_type].camelize.singularize.constantize

      @parent = klass.find(params[:parent_id])
    end

    private

    def affiliation_authenticate_user!
      authenticate_user!
    end
  end
end
