# frozen_string_literal: true

class InviteStrangerMailer < ApplicationMailer
  layout 'external_mailer'

  def invite_stranger(invitor, object, to_email)
    @invitor = invitor
    @object = object
    @object_type = object.class.name.casecmp('workgroup').zero? ? 'group' : object.class.name.downcase

    # Postmark metadatas
    metadata['object-type'] = @object_type

    mail(
      to: "<#{to_email}>",
      subject: "You have been invited to join a #{@object_type}",
      tag: 'invite-stranger'
    )
  end
end
