# coding: utf-8
# frozen_string_literal: true

class RecsysMailer < ApplicationMailer
  layout 'beta_mailer'

  @sanitizer = Rails::Html::WhiteListSanitizer.new

  def sanitize(string)
    @sanitizer.sanitize(string, tags: [])
  end

  def location(need)
    return "#{need.city}, #{need.country}" unless need.city.nil? || need.country.nil?
    return need.city.to_s unless need.city.nil?
    return need.country.to_s unless need.country.nil?

    ''
  end

  def recsys_recommendation(to, campaign_id, needs, scores)
    @campaign_id = campaign_id
    @needs = needs
    @scores = scores
    @to = to

    # Postmark metadatas
    metadata['user-id-to'] = to.id
    metadata['needs'] = needs.map(&:id).join(', ')
    metadata['campaign_id'] = campaign_id
    metadata['scores'] = scores.join(', ')

    mail(
      to: "#{to.full_name} <#{to.email}>",
      from: "JOGL - Just One Giant Lab <recommendations@#{Rails.configuration.email}>",
      subject: "We've been thinking about you! 👋",
      tag: 'needs-recommendations',
      message_stream: 'recommendations',
      &:html
    )
  end
end
