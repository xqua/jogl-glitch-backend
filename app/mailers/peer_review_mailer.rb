# frozen_string_literal: true

class PeerReviewMailer < ApplicationMailer
  def send_private_email(from, to, peer_review, content, subject)
    @content = content
    @from = from
    @peer_review = peer_review
    @to = to

    subject ||= "New message from peer review #{@peer_review.title}"

    # Postmark metadatas
    metadata['user-id-from'] = @from.id
    metadata['user-id-to'] = @to.id

    mail(
      to: "#{@to.full_name} <#{@to.email}>",
      from: "JOGL - Just One Giant Lab <notifications@#{Rails.configuration.email}>",
      subject: subject,
      tag: 'peer-review-mass-email'
    )
  end
end
