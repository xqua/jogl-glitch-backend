# frozen_string_literal: true

class SpaceMailer < ApplicationMailer
  def send_private_email(from, to, space, content, subject)
    @content = content
    @from = from
    @space = space
    @to = to

    subject ||= "New message from space #{@space.title}"

    # Postmark metadatas
    metadata['user-id-from'] = @from.id
    metadata['user-id-to'] = @to.id

    mail(
      to: "#{@to.full_name} <#{@to.email}>",
      from: "JOGL - Just One Giant Lab <notifications@#{Rails.configuration.email}>",
      subject: subject,
      tag: 'space-mass-email'
    )
  end
end
