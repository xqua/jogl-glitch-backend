# frozen_string_literal: true

module Types
  class UserType < Types::BaseObject
    field :id, ID, null: false
    # field :email, String, null: false
    # field :encrypted_password, String, null: false
    # field :reset_password_token, String, null: true
    # field :reset_password_sent_at, GraphQL::Types::ISO8601DateTime, null: true
    # field :remember_created_at, GraphQL::Types::ISO8601DateTime, null: true
    # field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    # field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    # field :first_name, String, null: true
    # field :last_name, String, null: true
    # field :nickname, String, null: true
    # field :age, Integer, null: true
    # field :social_cat, String, null: true
    # field :profession, String, null: true
    # field :country, String, null: true
    # field :city, String, null: true
    # field :address, String, null: true
    # field :phone_number, String, null: true
    # field :bio, String, null: true
    # field :allow_password_change, Boolean, null: true
    # field :feed_id, Integer, null: true
    # field :email_confirmed, Boolean, null: true
    # field :confirm_token, String, null: true
    # field :locked_at, GraphQL::Types::ISO8601DateTime, null: true
    # field :provider, String, null: false
    # field :uid, String, null: false
    # field :tokens, String, null: true
    # field :sign_in_count, Integer, null: false
    # field :current_sign_in_at, GraphQL::Types::ISO8601DateTime, null: true
    # field :last_sign_in_at, GraphQL::Types::ISO8601DateTime, null: true
    # field :current_sign_in_ip, Types::InetType, null: true
    # field :last_sign_in_ip, Types::InetType, null: true
    # field :confirmation_token, String, null: true
    # field :confirmed_at, GraphQL::Types::ISO8601DateTime, null: true
    # field :confirmation_sent_at, GraphQL::Types::ISO8601DateTime, null: true
    # field :unconfirmed_email, String, null: true
    # field :active_status, Integer, null: true
    # field :latitude, Float, null: true
    # field :longitude, Float, null: true
    # field :banner_url, String, null: true
    # field :affiliation, String, null: true
    # field :category, String, null: true
    # field :mail_newsletter, Boolean, null: true
    # field :mail_weekly, Boolean, null: true
    # field :short_bio, String, null: true
    # field :can_contact, Boolean, null: true
    # field :ip, String, null: true
    # field :sash_id, Integer, null: true
    # field :level, Integer, null: true
    # field :settings, String, null: true
    # field :status, String, null: true

    field :notifications, NotificationsWithCountsConnection, null: true

    def notifications
      object.notifications.order(created_at: :desc)
    end
  end
end
