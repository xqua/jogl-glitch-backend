# frozen_string_literal: true

module Types
  class NotificationType < Types::BaseObject
    field :id, ID, null: false
    field :target_type, String, null: true
    field :target_id, Integer, null: true
    field :object_type, String, null: true
    field :object_id, Integer, null: true, resolver_method: :resolve_object_id
    field :read, Boolean, null: false
    field :metadata, String, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :type, String, null: true
    field :subscription_id, Integer, null: true
    field :category, String, null: true
    field :cta_link, String, null: true

    field :subject_line, String, null: true

    def resolve_object_id
      object.object_id
    end
  end
end
